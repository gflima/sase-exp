#!/bin/sh -
run="java -cp target/sase-exp-1.0-SNAPSHOT-jar-with-dependencies.jar gflima.App"

exec $run $@

# examples
# ./run.sh --verbose --stream-iri http://example.org examples/who-likes-what.rq examples/like.stream
# ./run.sh --verbose --stream-iri http://example.org examples/who-dont-like.rq examples/like.stream
# ./run.sh --verbose --stream-iri http://example.org examples/who-likes-follows.rq examples/like.stream --static file:///home/gflima/src/csparql-cli/examples/follows.rdf
