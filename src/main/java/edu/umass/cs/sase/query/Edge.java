package edu.umass.cs.sase.query;

import java.util.StringTokenizer;

import edu.umass.cs.sase.engine.EventBuffer;
import edu.umass.cs.sase.engine.Run;
import edu.umass.cs.sase.stream.Event;

import net.sourceforge.jeval.EvaluationException;

/**
 * This class represents an edge of an NFA.
 * @author haopeng
 *
 */
//edgetype = begin & price < 100
public class Edge {
    /**
     * The original description in the nfa file
     */
    String edgeDescription;
    /**
     * The type of the edge, begin, , take or proceed
     */
    String edgeType;

    /**
     * Predicates on this edge
     */
    PredicateOptimized predicates[];

    /**
     * Constructor, based on the input string
     * @param edgeDescription the description sentence in the nfa file
     */
    public Edge(String edgeDescription){
        this.edgeDescription = edgeDescription;
        StringTokenizer st = new StringTokenizer(edgeDescription, "&");
        int size = st.countTokens();
        parseEdgeType(st.nextToken().trim());

        predicates = new PredicateOptimized[size -1];

        int count = 0;
        while(st.hasMoreTokens()){
            predicates[count] = new PredicateOptimized(st.nextToken().trim());
            count ++;
        }
    }

    public Edge(int edgeTypeNum){
        this.predicates = new PredicateOptimized[0];
        switch(edgeTypeNum){
        case 0: this.edgeType = "begin";
            break;
        case 1: this.edgeType = "take";
            break;
        case 2: this.edgeType = "proceed";
            break;
        }
    }
    public void addPredicate(String predicateDescription){

        PredicateOptimized newPredicates[] = new PredicateOptimized[this.predicates.length + 1];
        for(int i = 0; i < this.predicates.length; i ++){
            newPredicates[i] = this.predicates[i];
        }
        newPredicates[newPredicates.length - 1] = new PredicateOptimized(predicateDescription);
        this.predicates = newPredicates;

    }

    /**
     * Evaluates an event on this edge
     * @param currentEvent current event
     * @param previousEvent the previous event
     * @return the evaluation result.
     * @throws EvaluationException
     */
    public boolean evaluatePredicate(Event currentEvent, Event previousEvent) throws EvaluationException{
        for(int i = 0; i < this.predicates.length; i ++){

            if(!this.predicates[i].evaluate(currentEvent, previousEvent)){
                return false;

            }}
        return true;
    }

    /**
     * Override method, evaluates event based on the current event, and a run.
     * @param currentEvent the current event
     * @param r a run
     * @return the evaluate result
     */
    public boolean evaluatePredicate(Event currentEvent, Run r, EventBuffer b){
        for(int i = 0; i < this.predicates.length; i ++){
            try {
                if(!this.predicates[i].evaluate(currentEvent, r, b)){
                    return false;
                }
            } catch (EvaluationException e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    /**
     * Parses the edge from the input string
     * @param edgeTypeDescription the description string
     */
    void parseEdgeType(String edgeTypeDescription){
        StringTokenizer st = new StringTokenizer(edgeTypeDescription, "=");
        String left = st.nextToken().trim();
        String right = st.nextToken().trim();
        if(left.equalsIgnoreCase("edgetype"))
            this.edgeType = right;
    }

    public void dump () {
        System.out.println ("Edge type: " + this.edgeType);
        System.out.println ("      Description: " + this.edgeDescription);
        System.out.println ("      Number of predicates: " + this.predicates.length);
        for (int i = 0; i < this.predicates.length; i++) {
            PredicateOptimized p = this.predicates[i];
            String f = p.getFormatedPredicate ();
            System.out.println ("      " + i + ": " + f);
        }
    }

    public String getEdgeDescription() {
        return edgeDescription;
    }

    public void setEdgeDescription(String edgeDescription) {
        this.edgeDescription = edgeDescription;
    }

    public String getEdgeType() {
        return edgeType;
    }

    public void setEdgeType(String edgeType) {
        this.edgeType = edgeType;
    }

    public PredicateOptimized[] getPredicates() {
        return predicates;
    }

    public void setPredicates(PredicateOptimized[] predicates) {
        this.predicates = predicates;
    }
}
