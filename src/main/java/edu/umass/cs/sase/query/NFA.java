package edu.umass.cs.sase.query;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.StringReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.StringTokenizer;

import edu.umass.cs.sase.engine.ConfigFlags;
import edu.umass.cs.sase.engine.Run;

public class NFA {
    State[] states;

    int size = 0;               // number of states

    String selectionStrategy;

    int timeWindow;

    // Whether query needs value vector.
    // It is needed when there are aggregates or parameterized predicates.
    boolean needValueVector;

    // Value vectors for the computation state.
    ValueVectorTemplate[][] valueVectors;

    // Whether the query has value vectors.
    boolean hasValueVector[];

    // Specifies the partiton attribute.
    // Used only when with partition-contiguity selection strategy.
    String partitionAttribute;

    // Whether the query has a partition attribute.
    boolean hasPartitionAttribute;

    // Store other partition attributes except for the first one, if any.
    ArrayList<String> morePartitionAttribute;

    // Whether query has more than one partition attributes.
    boolean hasMorePartitionAttribute;

    // Whether query has a negation component.
    boolean hasNegation;

    // The negation state.
    State negationState;

    public NFA (String selectionStrategy, String nfaFile) {
        this.selectionStrategy = selectionStrategy;
        parseNfaFile (nfaFile);
        this.testNegation ();
        this.compileValueVectorOptimized ();
    }

    /**
     * Parses the nfa file
     * @param nfaFile the nfa file
     */
    public void parseNfaFile (String query) {
        String line = "";
        try {
            BufferedReader br = new BufferedReader(new StringReader(query));
            line = br.readLine();
            if(line.startsWith("SelectionStrategy") || line.startsWith("selectionStrategy")){
                // parse the descriptive format
                // next line, parse the configuration parameters
                parseNfaConfig(line);
                // count the size of nfa
                while ((line = br.readLine())!=null){
                    if (line.equalsIgnoreCase("end"))
                        break;
                    else
                        size ++;
                }
                states = new State[size];

                //parse each state
                br = new BufferedReader(new StringReader(query));
                br.readLine();// pass the configuration line;

                int counter = 0;
                while((line = br.readLine())!=null){

                    if (line.equalsIgnoreCase("end")) // reads the end of nfa file
                        break;
                    else
                        {
                            states[counter] = new State(line.trim(), counter);// starts with 0
                            counter ++;
                        }

                }
            }
            else if(line.startsWith("PATTERN")){
                // parse the simpler format
                this.morePartitionAttribute = new ArrayList<String>();
                this.hasMorePartitionAttribute = false;
                do {
                    this.parseFastQueryLine(line);
                }
                while((line = br.readLine()) != null);
                if(this.hasMorePartitionAttribute){
                    this.addMorePartitionAttribute();
                }
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if(this.size > 0){
            states[0].setStart(true);
            states[size-1].setEnding(true);
        }
    }

    /**
     * Parses each line for the fast query format
     * @param line
     */
    public void parseFastQueryLine(String line){
        if(line.startsWith("PATTERN")){
            this.parseFastQueryLineStartWithPattern(line);
        }else if(line.startsWith("WHERE")){
            // parse the selection strategy
            StringTokenizer st = new StringTokenizer(line);
            st.nextToken();
            if (this.selectionStrategy == null) {
                this.selectionStrategy = st.nextToken().trim();
            }
        }else if(line.startsWith("AND")){
            this.parseFastQueryLineStartWithAND(line);
        }else if(line.startsWith("WITHIN")){
            // parse the time window
            StringTokenizer st = new StringTokenizer(line);
            st.nextToken();
            this.timeWindow = Integer.parseInt(st.nextToken().trim());
        }
    }

    /**
     * Parses the query sequence
     * @param line
     */
    public void parseFastQueryLineStartWithPattern(String line){
        String seq = line.substring(line.indexOf('(') + 1, line.indexOf(')'));
        StringTokenizer st = new StringTokenizer(seq, ",");
        this.size = st.countTokens();
        //System.out.println(size);
        this.states = new State[size];
        String state;
        for(int i = 0; i < size; i ++){
            boolean isKleeneClosure = false;
            boolean isNegation = false;
            state = st.nextToken();

            StringTokenizer stateSt = new StringTokenizer(state);
            String eventType = stateSt.nextToken().trim();
            String stateTag = stateSt.nextToken().trim();
            if(eventType.contains("+")){
                isKleeneClosure = true;
                eventType = eventType.substring(0, eventType.length() - 1);// the first letter
                stateTag = stateTag.substring(0, 1);
            }else if(eventType.contains("!")){
                // System.out.println ("AAAAAAAAAAAAAAAAAAAA");
                isNegation = true;
                eventType = eventType.substring(1, eventType.length());

            }

            //System.out.println("The tag for state " + i + " is " + stateTag);
            if(isKleeneClosure){

                this.states[i] = new State(i + 1, stateTag, eventType, "kleeneClosure");
            }else if(isNegation){
                this.states[i] = new State(i + 1, stateTag, eventType, "negation");
            }else{
                this.states[i] = new State (i + 1, stateTag, eventType, "normal");
            }
        }
    }

    /**
     * Parses the conditions starting with "AND", it might be the partition
     * attribute, or predicates for states
     * @param line
     */
    public void parseFastQueryLineStartWithAND(String line){
        StringTokenizer st = new StringTokenizer(line);
        st.nextToken();
        String token = st.nextToken().trim();
        if(token.startsWith("[")){
            //the partition attribute
            if(!this.hasPartitionAttribute){
                this.partitionAttribute = token.substring(1, token.length() - 1);
                this.hasPartitionAttribute = true;
            }else{
                this.hasMorePartitionAttribute = true;
                this.morePartitionAttribute.add(token.substring(1, token.length() - 1));
            }
        }else {
            char initial = token.charAt(0);
            int stateNum = initial - 'a';//determine which state this predicate works for according to the initial
            this.states[stateNum].addPredicate(line.substring(3).trim());
        }
        //todo for states

    }
    /**
     * Adds other partition attributes except for the first to each state
     */
    public void addMorePartitionAttribute(){
        String tempPredicate;
        for(int i = 0; i < this.morePartitionAttribute.size(); i ++){
            tempPredicate = this.morePartitionAttribute.get(i)+"=$1." + this.morePartitionAttribute.get(i);//?
            for(int j = 1; j < this.size; j ++){
                State tempState = this.getStates(j);
                for(int k = 0; k < tempState.getEdges().length; k ++){
                    Edge tempEdge = tempState.getEdges(k);
                    tempEdge.addPredicate(tempPredicate);
                }
            }
        }
    }
    /**
     * Parses the configuration line in the nfa file
     * @param line
     */
    public void parseNfaConfig(String line){
        StringTokenizer st = new StringTokenizer(line, "|");
        while(st.hasMoreTokens()){
            parseConfig(st.nextToken().trim());
        }

    }

    /**
     * Parses a configuration, now we have selection strategy, time window
     * and partiton attribute
     * @param attribute a configuration
     */
    public void parseConfig(String attribute){
        StringTokenizer st = new StringTokenizer(attribute, "=");
        String left = st.nextToken().trim();
        String right = st.nextToken().trim();
        if(left.equalsIgnoreCase("selectionStrategy")){
            this.selectionStrategy = right;
        }else if(left.equalsIgnoreCase("timeWindow")){
            this.timeWindow = Integer.parseInt(right);
        }else if(left.equalsIgnoreCase("partitionAttribute")){
            this.partitionAttribute = right;
            ConfigFlags.partitionAttribute = right;
            this.hasPartitionAttribute = true;
        }
    }

    /**
     * Tests whether the query contains a negation component
     */
    public void testNegation(){
        for(int i = 0; i < this.size; i ++){
            if(this.getStates(i).stateType.equalsIgnoreCase("negation")){
                this.hasNegation = true;
                this.negationState = this.getStates(i);
            }
        }
        if(this.hasNegation){
            int negationOrder = this.negationState.getOrder()- 1;
            State newState[] = new State[this.size - 1];
            for(int i = 0; i < this.size - 1; i ++){
                if(i < negationOrder){
                    newState[i] = this.getStates(i);
                    if(i == negationOrder - 1){
                        newState[i].setBeforeNegation(true);
                    }
                }else {
                    newState[i] = this.getStates(i + 1);
                    if(i == negationOrder ){
                        newState[i].setAfterNegation(true);
                    }

                }
            }
            this.size = this.size - 1;
            this.setStates(newState);
        }
    }

    /**
     * Compiles the value vector based on the nfa
     */
    public void compileValueVectorOptimized(){
        this.valueVectors = new ValueVectorTemplate[this.size][];
        ArrayList<ValueVectorTemplate> valueV = new ArrayList<ValueVectorTemplate>();
        int counter[] = new int[this.size];
        for(int i = 0; i < this.size; i ++){
            counter[i] = 0;
        }
        for(int i = 0; i < this.getSize(); i ++){
            State tempState = this.getStates(i);

            for(int j = 0; j < tempState.getEdges().length; j ++){
                Edge tempEdge = tempState.getEdges(j);
                for(int k = 0; k < tempEdge.getPredicates().length; k ++){
                    PredicateOptimized tempPredicate = tempEdge.getPredicates()[k];
                    if(!tempPredicate.isSingleState()){
                        String operationName = tempPredicate.getOperation();
                        String attributeName = tempPredicate.getAttributeName();
                        int stateNumber;
                        if(tempPredicate.getRelatedState().equals("previous")){
                            stateNumber = i - 1;
                        }else{
                            stateNumber = Integer.parseInt(tempPredicate.getRelatedState()) - 1;
                        }
                        valueV.add(new ValueVectorTemplate(stateNumber,attributeName, operationName,i));
                        counter[stateNumber]++;

                    }
                }
            }

        }

        //set the needValueVector flag as true
        if(valueV.size() > 0){
            this.needValueVector = true;
        }
        //put the value vector tempate to each state
        for(int i = 0; i < this.size ; i ++){
            this.valueVectors[i] = new ValueVectorTemplate[counter[i]];
            ValueVectorTemplate temp;
            int count = 0;
            for(int j = 0; j < valueV.size(); j ++){
                temp = valueV.get(j);
                if(temp.getState() == i){
                    this.valueVectors[i][count] = temp;
                    count ++;
                }
            }
        }
        this.hasValueVector = new boolean[this.size];
        for(int i = 0; i < this.size; i ++){
            if(counter[i]>0){
                this.hasValueVector[i] = true;
            }else{
                this.hasValueVector[i] = false;
            }
        }

    }

    public void dump () {
        System.out.println ("--- automaton begin ---");
        System.out.println ("Selection strategy: " + this.selectionStrategy);
        System.out.println ("Time window: " + this.timeWindow);
        if (this.hasPartitionAttribute == true) {
            System.out.println ("Partition attribute: " + this.partitionAttribute);
        }
        System.out.println ("Number of states: " + this.states.length);
        for (int i = 0; i < this.states.length; i++) {
            State s = this.states[i];
            System.out.print ("\n#" + i);
            s.dump ();
        }
        System.out.println ("--- automaton end --- ");
    }

    public State[] getStates() {
        return states;
    }

    public State getStates (int order) {
        return states[order];
    }

    public void setStates(State[] states) {
        this.states = states;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getSelectionStrategy() {
        return selectionStrategy;
    }

    public void setSelectionStrategy(String selectionStrategy) {
        this.selectionStrategy = selectionStrategy;
    }

    public int getTimeWindow() {
        return timeWindow;
    }

    public void setTimeWindow(int timeWindow) {
        this.timeWindow = timeWindow;
    }

    public boolean isNeedValueVector() {
        return needValueVector;
    }

    public void setNeedValueVector(boolean needValueVector) {
        this.needValueVector = needValueVector;
    }

    public String getPartitionAttribute() {
        return partitionAttribute;
    }

    public void setPartitionAttribute(String partitionAttribute) {
        this.partitionAttribute = partitionAttribute;
    }

    public ValueVectorTemplate[][] getValueVectors() {
        return valueVectors;
    }

    public void setValueVectors(ValueVectorTemplate[][] valueVectors) {
        this.valueVectors = valueVectors;
    }

    public boolean[] getHasValueVector() {
        return hasValueVector;
    }

    public void setHasValueVector(boolean[] hasValueVector) {
        this.hasValueVector = hasValueVector;
    }

    public boolean isHasPartitionAttribute() {
        return hasPartitionAttribute;
    }

    public void setHasPartitionAttribute(boolean hasPartitionAttribute) {
        this.hasPartitionAttribute = hasPartitionAttribute;
    }

    public boolean isHasNegation() {
        return hasNegation;
    }

    public void setHasNegation(boolean hasNegation) {
        this.hasNegation = hasNegation;
    }

    public State getNegationState() {
        return negationState;
    }

    public void setNegationState(State negationState) {
        this.negationState = negationState;
    }
}
