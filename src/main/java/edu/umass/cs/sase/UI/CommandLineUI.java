package edu.umass.cs.sase.UI;

import edu.umass.cs.sase.engine.Engine;
import edu.umass.cs.sase.query.NFA;
import edu.umass.cs.sase.stream.ABCEvent;
import edu.umass.cs.sase.stream.Event;
import edu.umass.cs.sase.stream.StockEvent;
import edu.umass.cs.sase.stream.Stream;
import java.util.Random;

import edu.umass.cs.sase.engine.ConfigFlags;
import edu.umass.cs.sase.engine.Profiling;
import java.io.IOException;
import net.sourceforge.jeval.EvaluationException;

public class CommandLineUI {

    // Q1 ------------------------------------------------------------------

    static String Q1 = "PATTERN SEQ(stock+ a[], stock b)\n"
        + "WHERE skip-till-next-match\n"
        + "AND [symbol]\n"
        + "AND a[1].price % 500 = 0\n"
        + "AND a[i].price > a[i-1].price\n"
        + "AND b.volume < 150\n"
        + "WITHIN 500\n";

    static Event[] genQ1Events () {
        // Random rand = new Random (11);
        // StockEvent evts[] = new StockEvent[n];
        // for (int i = 0; i < n; i ++){
        //     int sym = rand.nextInt (2) + 1;
        //     evts[i] = new StockEvent
        //         (i,                       // id
        //          i,                       // timestamp
        //          sym,                     // symbol
        //          rand.nextInt (1000) + 1, // price
        //          rand.nextInt (1000) + 1, // volume
        //          "stock" + sym);          // type
        // }
        // return evts;
        StockEvent[] evts = {
            new StockEvent (199, 199, 1, 201, 159, "stock"),
            new StockEvent (200, 200, 2, 500, 656, "stock"),
            new StockEvent (201, 201, 2, 503, 437, "stock"),
            new StockEvent (202, 202, 1, 204, 552, "stock"),
            new StockEvent (203, 203, 2, 503, 319, "stock"),
            new StockEvent (204, 204, 2, 506, 829, "stock"),
            new StockEvent (205, 205, 1, 206, 293, "stock"),
            new StockEvent (206, 206, 2, 509, 499, "stock"),
            new StockEvent (207, 207, 2, 509, 160, "stock"),
            new StockEvent (208, 208, 2, 510, 636, "stock"),
            new StockEvent (209, 209, 2, 511, 972, "stock"),
            new StockEvent (210, 210, 1, 206, 385, "stock"),
            new StockEvent (211, 211, 1, 207, 905, "stock"),
            new StockEvent (212, 212, 2, 514, 969, "stock"),
            new StockEvent (213, 213, 2, 515, 523, "stock"),
            new StockEvent (214, 214, 1, 208, 123, "stock"),
            new StockEvent (215, 215, 1, 205, 856, "stock"),
            new StockEvent (216, 216, 1, 205, 146, "stock"),
            new StockEvent (217, 217, 1, 207, 961, "stock"),
            new StockEvent (218, 218, 1, 208, 403, "stock"),
            new StockEvent (219, 219, 1, 207, 933, "stock"),
            new StockEvent (220, 220, 1, 210, 838, "stock"),
            new StockEvent (221, 221, 2, 512, 138, "stock"),
            new StockEvent (222, 222, 1, 210, 44, "stock"),
            new StockEvent (223, 223, 1, 210, 341, "stock"),
            new StockEvent (224, 224, 2, 514, 641, "stock"),
            new StockEvent (225, 225, 2, 515, 866, "stock"),
            new StockEvent (226, 226, 2, 518, 234, "stock"),
            new StockEvent (227, 227, 2, 516, 526, "stock"),
            new StockEvent (228, 228, 1, 212, 428, "stock"),
            new StockEvent (229, 229, 1, 215, 309, "stock"),
            new StockEvent (230, 230, 1, 215, 322, "stock"),
        };
        return evts;
    }

    static Event[] Q1_evts = genQ1Events ();

    // Q2 ------------------------------------------------------------------
    static String Q2 = "PATTERN SEQ(A a, B b, C c)\n"
        + "WHERE skip-till-next-match\n"
        + "WITHIN 10\n";

    static Event[] Q2_evts = {
        new ABCEvent (1, 1, "A", 1),
        new ABCEvent (2, 2, "A", 2),
        new ABCEvent (3, 3, "B", 3),
        new ABCEvent (4, 4, "B", 4),
        new ABCEvent (5, 5, "C", 5),
        new ABCEvent (6, 6, "C", 6),
        new ABCEvent (7, 7, "A", 7),
    };

    // Main ----------------------------------------------------------------

    public static void main (String args[])
        throws CloneNotSupportedException, EvaluationException, IOException {

        NFA nfa = null;
        Stream stream = null;

        if (args.length < 1) {
            System.err.println ("Usage: sase QUERY");
            System.exit (1);
        }

        String query = args[0];
        String strat = null;
        if (args.length >= 2) {
            strat = args[1];
        }

        if (query.equals ("Q1")) {
            nfa = new NFA (strat, Q1);
            stream = new Stream (Q1_evts);
        } else if (query.equals ("Q2")) {
            nfa = new NFA (strat, Q2);
            stream = new Stream (Q2_evts);
        } else {
            System.err.println
                (String.format ("error: Unknown query %s", args[0]));
            System.exit (1);
        }

        // nfa.dump ();
        // System.out.println("----------Here is the input stream----------\n");
        // for(int i = 0; i < stream.getSize (); i ++){
        //     Event e = stream.getEvents ()[i];
        //     String s = e.toString ();
        //     System.out.println(s);
        // }
        // System.out.println ("");

        Engine engine = new Engine (nfa, stream);
        System.gc ();
        engine.run ();
        Profiling.printProfiling ();
    }
}
