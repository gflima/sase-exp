package edu.umass.cs.sase.engine;

public class ConfigFlags {
    public static String selectionStrategy = "skip-till-any-match" ;
    public static boolean hasPartitionAttribute = false;

    // Name of the partition attribute.
    public static String partitionAttribute;

    // Whether the query has a negation component.
    public static boolean hasNegation = false;
}
