package edu.umass.cs.sase.engine;

import java.util.ArrayList;
import java.util.HashMap;

import net.sourceforge.jeval.EvaluationException;

import edu.umass.cs.sase.query.Edge;
import edu.umass.cs.sase.query.NFA;
import edu.umass.cs.sase.query.State;
import edu.umass.cs.sase.stream.Event;
import edu.umass.cs.sase.stream.Stream;

public class Engine {
    Stream input;
    EventBuffer buffer;
    NFA nfa;

    ArrayList<Run> runPool;

    ArrayList<Run> activeRuns;
    HashMap<Integer, ArrayList<Run>> activeRunsByPartition;

    // Runs which can be removed from the active runs.
    ArrayList<Run> toDeleteRuns;

    // Matches.
    MatchController matches;

    // Buffered events for the negation components
    ArrayList<Event> negationEvents;

    HashMap<Integer, ArrayList<Event>> negationEventsByPartition;

    public Engine (NFA nfa, Stream input){
        this.nfa = nfa;
        this.input = input;
        this.buffer = new EventBuffer();
        this.runPool = new ArrayList<Run>();
        this.activeRuns = new ArrayList<Run>();
        this.activeRunsByPartition = new HashMap<Integer, ArrayList<Run>>();
        this.toDeleteRuns = new ArrayList<Run>();
        this.matches = new MatchController();
        Profiling.resetProfiling ();
    }

    public void run() throws CloneNotSupportedException, EvaluationException{
        ConfigFlags.selectionStrategy = this.nfa.getSelectionStrategy();
        ConfigFlags.hasPartitionAttribute = this.nfa.isHasPartitionAttribute();
        ConfigFlags.hasNegation = this.nfa.isHasNegation();

        System.out.println ("Engine.run()");
        System.out.println ("\tselection strategy: " + ConfigFlags.selectionStrategy);
        System.out.println ("\tpartition attribute: " + ConfigFlags.hasPartitionAttribute);
        System.out.println ("\tnegation: " + ConfigFlags.hasNegation);
        System.out.println ("");

        if(ConfigFlags.hasNegation){
            this.runNegationEngine();
        }else if(ConfigFlags.selectionStrategy.equalsIgnoreCase("skip-till-any-match")){
            this.runSkipTillAnyEngine();
        }else if(ConfigFlags.selectionStrategy.equalsIgnoreCase("skip-till-next-match")){
            this.runSkipTillNextEngine();
        }else if(ConfigFlags.selectionStrategy.equalsIgnoreCase("partition-contiguity")){
            this.runPartitionContiguityEngine();
        }
    }

    public void runSkipTillAnyEngine()
        throws CloneNotSupportedException, EvaluationException {
        System.out.println ("> Engine.runSkipTillAnyEngine()");
        if(!ConfigFlags.hasPartitionAttribute){
            Event e = null;
            long currentTime = 0;
            while ((e = this.input.popEvent())!= null){// evaluate event one by one
                currentTime = System.nanoTime();
                this.evaluateRunsForSkipTillAny(e);// evaluate existing runs
                if(this.toDeleteRuns.size() > 0){
                    this.cleanRuns();
                }
                this.createNewRun(e); // create new run starting with this event if possible


                Profiling.totalRunTime += (System.nanoTime() - currentTime);
                Profiling.numberOfEvents += 1;

            }
        }

        if(ConfigFlags.hasPartitionAttribute){
            ConfigFlags.partitionAttribute = this.nfa.getPartitionAttribute();
            this.activeRunsByPartition = new HashMap<Integer, ArrayList<Run>>();
            Event e = null;
            long currentTime = 0;
            while ((e = this.input.popEvent())!= null){// evaluate event one by one
                currentTime = System.nanoTime();
                this.evaluateRunsByPartitionForSkipTillAny(e);// evaluate existing runs
                if(this.toDeleteRuns.size() > 0){
                    this.cleanRunsByPartition();
                }
                this.createNewRunByPartition(e);// create new run starting with this event if possible


                Profiling.totalRunTime += (System.nanoTime() - currentTime);
                Profiling.numberOfEvents += 1;

            }
        }
    }

    public void runSkipTillNextEngine()
        throws CloneNotSupportedException, EvaluationException{
        System.out.println ("> Engine.runSkipTillNextEngine()");
        if(!ConfigFlags.hasPartitionAttribute){
            Event e = null;
            long currentTime = 0;
            while ((e = this.input.popEvent())!= null){// evaluate event one by one
                currentTime = System.nanoTime();
                this.evaluateRunsForSkipTillNext(e);// evaluate existing runs
                if(this.toDeleteRuns.size() > 0){
                    this.cleanRuns();
                }
                this.createNewRun(e);// create new run starting with this event if possible


                Profiling.totalRunTime += (System.nanoTime() - currentTime);
                Profiling.numberOfEvents += 1;

            }
        }

        if(ConfigFlags.hasPartitionAttribute){
            ConfigFlags.partitionAttribute = this.nfa.getPartitionAttribute();
            this.activeRunsByPartition = new HashMap<Integer, ArrayList<Run>>();
            Event e = null;
            long currentTime = 0;
            while ((e = this.input.popEvent())!= null){// evaluate event one by one
                currentTime = System.nanoTime();
                //System.out.println(e.toString ());
                this.evaluateRunsByPartitionForSkipTillNext(e);// evaluate existing runs
                if(this.toDeleteRuns.size() > 0){
                    this.cleanRunsByPartition();
                }
                this.createNewRunByPartition(e);  // create new run starting with this event if possible
                Profiling.totalRunTime += (System.nanoTime() - currentTime);
                Profiling.numberOfEvents += 1;
            }
        }
    }

    public void runPartitionContiguityEngine()
        throws EvaluationException, CloneNotSupportedException{
        System.out.println ("> Engine.runPartitionContiguityEngine()");
        ConfigFlags.partitionAttribute = this.nfa.getPartitionAttribute();
        ConfigFlags.hasPartitionAttribute = true;
        this.activeRunsByPartition = new HashMap<Integer, ArrayList<Run>>();
        ConfigFlags.selectionStrategy = this.nfa.getSelectionStrategy();

        Event e = null;
        long currentTime = 0;
        while((e = this.input.popEvent())!= null){

            currentTime = System.nanoTime();
            this.evaluateRunsForPartitionContiguity(e);
            if(this.toDeleteRuns.size() > 0){
                this.cleanRunsByPartition();
            }
            this.createNewRunByPartition(e);
            Profiling.totalRunTime += (System.nanoTime() - currentTime);
            Profiling.numberOfEvents += 1;
        }
    }

    public void runNegationEngine()
        throws CloneNotSupportedException, EvaluationException{
        System.out.println ("> Engine.runNegationEngine()");
        if(!ConfigFlags.hasPartitionAttribute) {
            Event e = null;
            long currentTime = 0;
            this.negationEvents = new ArrayList<Event>();
            while ((e = this.input.popEvent())!= null){// evaluate event one by one
                currentTime = System.nanoTime();
                if(this.checkNegation(e)){
                    this.negationEvents.add(e);
                }else{
                    this.evaluateRunsForNegation(e);// evaluate existing runs
                    if(this.toDeleteRuns.size() > 0){
                        this.cleanRuns();
                    }
                    this.createNewRun(e);// create new run starting with this event if possible
                }

                Profiling.totalRunTime += (System.nanoTime() - currentTime);
                Profiling.numberOfEvents += 1;

            }
        }

        if(ConfigFlags.hasPartitionAttribute) {
            ConfigFlags.partitionAttribute = this.nfa.getPartitionAttribute();
            this.activeRunsByPartition = new HashMap<Integer, ArrayList<Run>>();
            this.negationEventsByPartition = new HashMap<Integer, ArrayList<Event>>();

            Event e = null;
            long currentTime = 0;
            while ((e = this.input.popEvent())!= null){// evaluate event one by one
                currentTime = System.nanoTime();
                if(this.checkNegation(e)){
                    this.indexNegationByPartition(e);
                }else{
                    this.evaluateRunsByPartitionForNegation(e);// evaluate existing runs
                    if(this.toDeleteRuns.size() > 0){
                        this.cleanRunsByPartition();
                    }
                    this.createNewRunByPartition(e);// create new run starting with this event if possible
                }

                Profiling.totalRunTime += (System.nanoTime() - currentTime);
                Profiling.numberOfEvents += 1;

            }
        }
    }


    public void evaluateRunsForSkipTillAny(Event e)
        throws CloneNotSupportedException, EvaluationException{
        System.out.println (String.format (">> Engine.evaluateRunsForSkipTillAny(%s)", e));
        int size = this.activeRuns.size();
        for(int i = 0; i < size; i ++){
            Run r = this.activeRuns.get(i);
            if(r.isFull()){
                continue;
            }
            this.evaluateEventForSkipTillAny(e, r);
        }
    }

    public void evaluateRunsForSkipTillNext(Event e)
        throws CloneNotSupportedException, EvaluationException{
        System.out.println (String.format (">> Engine.evaluateRunsForSkipTillNext(%s)", e));
        int size = this.activeRuns.size();
        for(int i = 0; i < size; i ++){
            Run r = this.activeRuns.get(i);
            if(r.isFull()) {
                continue;
            }
            this.evaluateEventForSkipTillNext(e, r);
        }
    }

    public void evaluateRunsForNegation(Event e)
        throws CloneNotSupportedException, EvaluationException{
        System.out.println (String.format (">> Engine.evaluateRunsForNegation(%s)", e));
        int size = this.activeRuns.size();
        for(int i = 0; i < size; i ++){
            Run r = this.activeRuns.get(i);
            if(r.isFull()){
                continue;
            }
            this.evaluateEventForNegation(e, r);
        }
    }

    public void evaluateRunsByPartitionForSkipTillAny(Event e)
        throws CloneNotSupportedException{
        System.out.println (String.format (">> Engine.evaluateRunsByPartitionForSkipTillAny(%s)", e));
        int key = e.getAttributeByName(ConfigFlags.partitionAttribute);
        if(this.activeRunsByPartition.containsKey(key)){
            ArrayList<Run> partitionedRuns = this.activeRunsByPartition.get(key);
            int size = partitionedRuns.size();
            for(int i = 0; i < size; i ++){
                Run r = partitionedRuns.get(i);
                if(r.isFull()){
                    continue;
                }
                this.evaluateEventOptimizedForSkipTillAny(e, r);//
            }
        }
    }

    public void evaluateRunsByPartitionForSkipTillNext(Event e)
        throws CloneNotSupportedException{
        System.out.println (String.format (">> Engine.evaluateRunsByPartitionForSkipTillNext(%s)", e));
        int key = e.getAttributeByName(ConfigFlags.partitionAttribute);
        if(this.activeRunsByPartition.containsKey(key)){
            ArrayList<Run> partitionedRuns = this.activeRunsByPartition.get(key);
            int size = partitionedRuns.size();
            for(int i = 0; i < size; i ++){
                Run r = partitionedRuns.get(i);
                if(r.isFull()){
                    continue;
                }
                this.evaluateEventOptimizedForSkipTillNext(e, r);//
            }
        }
    }

    public void evaluateRunsByPartitionForNegation(Event e)
        throws CloneNotSupportedException{
        System.out.println (String.format (">> Engine.evaluateRunsByPartitionForNegation(%s)", e));
        int key = e.getAttributeByName(ConfigFlags.partitionAttribute);
        if(this.activeRunsByPartition.containsKey(key)){
            ArrayList<Run> partitionedRuns = this.activeRunsByPartition.get(key);
            int size = partitionedRuns.size();
            for(int i = 0; i < size; i ++){
                Run r = partitionedRuns.get(i);
                if(r.isFull()){
                    continue;
                }
                this.evaluateEventOptimizedForNegation(e, r);//
            }
        }
    }

    public void evaluateRunsForPartitionContiguity(Event e)
        throws CloneNotSupportedException{
        int key = e.getAttributeByName(ConfigFlags.partitionAttribute);
        if(this.activeRunsByPartition.containsKey(key)){
            ArrayList<Run> partitionedRuns = this.activeRunsByPartition.get(key);
            int size = partitionedRuns.size();
            for(int i = 0; i < size; i ++){
                Run r = partitionedRuns.get(i);
                if(r.isFull()){
                    continue;
                }
                this.evaluateEventForPartitonContiguityOptimized(e, r);//
            }
        }
    }

    public void evaluateEventOptimizedForSkipTillAny(Event e, Run r)
        throws CloneNotSupportedException{
        int checkResult = this.checkPredicateOptimized(e, r);
        switch(checkResult){
        case 1:
            boolean timeWindow = this.checkTimeWindow(e, r);
            if(timeWindow){
                Run newRun = this.cloneRun(r);
                this.addRunByPartition(newRun);

                this.addEventToRun(r, e);
            }else{
                this.toDeleteRuns.add(r);
            }
            break;
        case 2:
            Run newRun = this.cloneRun(r);
            this.addRunByPartition(newRun);

            r.proceed();
            this.addEventToRun(r, e);
        }
    }

    public void evaluateEventOptimizedForSkipTillNext(Event e, Run r){
        int checkResult = this.checkPredicateOptimized(e, r);
        switch(checkResult){
        case 1:
            boolean timeWindow = this.checkTimeWindow(e, r);
            if(timeWindow){
                this.addEventToRun(r, e);
            }else{
                this.toDeleteRuns.add(r);
            }
            break;
        case 2:
            r.proceed();
            this.addEventToRun(r, e);
        }
    }

    public void evaluateEventOptimizedForNegation(Event e, Run r)
        throws CloneNotSupportedException{
        int checkResult = this.checkPredicateOptimized(e, r);
        switch(checkResult){
        case 1:
            boolean timeWindow = this.checkTimeWindow(e, r);
            if(timeWindow){
                Run newRun = this.cloneRun(r);
                this.addRunByPartition(newRun);

                this.addEventToRunForNegation(r, e);
            }else{
                this.toDeleteRuns.add(r);
            }
            break;
        case 2:
            Run newRun = this.cloneRun(r);
            this.addRunByPartition(newRun);

            r.proceed();
            this.addEventToRunForNegation(r, e);
        }
    }

    public void evaluateEventForPartitonContiguityOptimized(Event e, Run r)
        throws CloneNotSupportedException{
        int checkResult = this.checkPredicateOptimized(e, r);
        switch(checkResult){
        case 0:
            this.toDeleteRuns.add(r);
            break;
        case 1:
            boolean timeWindow = this.checkTimeWindow(e, r);
            if(timeWindow){
                this.addEventToRun(r, e);
            }else{
                this.toDeleteRuns.add(r);
            }
            break;
        case 2:
            r.proceed();
            this.addEventToRun(r, e);
        }


    }

    public void evaluateEventForSkipTillAny(Event e, Run r)
        throws CloneNotSupportedException{
        boolean checkResult = true;

        checkResult = this.checkPredicate(e, r);// check predicate
        if(checkResult){ // the predicate if ok.
            checkResult = this.checkTimeWindow(e, r); // the time window is ok
            if(checkResult){// predicate and time window are ok
                this.buffer.bufferEvent(e);// add the event to buffer
                int oldState = 0;
                int newState = 0;
                Run newRun = this.cloneRun(r); // clone this run
                oldState = newRun.getCurrentState();
                newRun.addEvent(e);     // add the event to this run
                newState = newRun.getCurrentState();
                if(oldState != newState){
                    this.activeRuns.add(newRun);
                }else{//kleene closure
                    if(newRun.isFull){
                        //check match and output match
                        if(newRun.checkMatch()){
                            this.outputMatch(new Match(newRun, this.nfa, this.buffer));
                            Profiling.totalRunLifeTime += (System.nanoTime() - r.getLifeTimeBegin());

                        }
                    }else{
                        //check proceed
                        if(this.checkProceed(newRun)){
                            Run newerRun = this.cloneRun(newRun);
                            this.activeRuns.add(newRun);
                            newerRun.proceed();
                            if(newerRun.isComplete()){
                                this.outputMatch(new Match(r, this.nfa, this.buffer));
                                Profiling.totalRunLifeTime += (System.nanoTime() - r.getLifeTimeBegin());

                            }else {
                                this.activeRuns.add(newerRun);
                            }
                        }
                    }
                }



            }else{
                this.toDeleteRuns.add(r);
            }

        }
    }

    public void evaluateEventForSkipTillNext(Event e, Run r)
        throws CloneNotSupportedException{
        System.out.println (String.format (">>> Engine.evaluateEventForSkipTillNext(%s,%s)", e, r));
        boolean checkResult;
        checkResult = this.checkPredicate(e, r);// check predicate
        if(checkResult){ // the predicate if ok.
            checkResult = this.checkTimeWindow(e, r); // the time window is ok
            if(checkResult){// predicate and time window are ok
                this.buffer.bufferEvent(e);// add the event to buffer
                int oldState = 0;
                int newState = 0;


                oldState = r.getCurrentState();
                r.addEvent(e);
                newState = r.getCurrentState();
                if(oldState == newState)//kleene closure
                    if(r.isFull){
                        //check match and output match
                        if(r.checkMatch()){
                            this.outputMatch(new Match(r, this.nfa, this.buffer));
                            Profiling.totalRunLifeTime += (System.nanoTime() - r.getLifeTimeBegin());
                            this.toDeleteRuns.add(r);
                        }
                    }else{
                        //check proceed
                        if(this.checkProceed(r)){
                            Run newRun = this.cloneRun(r);

                            this.activeRuns.add(newRun);
                            this.addRunByPartition(newRun);
                            r.proceed();
                            if(r.isComplete()){
                                this.outputMatch(new Match(r, this.nfa, this.buffer));
                                Profiling.totalRunLifeTime += (System.nanoTime() - r.getLifeTimeBegin());
                                this.toDeleteRuns.add(r);

                            }
                        }
                    }


            }else{
                this.toDeleteRuns.add(r);
            }

        }
    }

    public void evaluateEventForNegation(Event e, Run r)
        throws CloneNotSupportedException {
        boolean checkResult = true;

        checkResult = this.checkPredicate(e, r);// check predicate
        if(checkResult){ // the predicate if ok.
            checkResult = this.checkTimeWindow(e, r); // the time window is ok
            if(checkResult){// predicate and time window are ok
                this.buffer.bufferEvent(e);// add the event to buffer
                int oldState = 0;
                int newState = 0;


                Run newRun = this.cloneRun(r); // clone this run


                oldState = newRun.getCurrentState();
                newRun.addEvent(e);     // add the event to this run
                newState = newRun.getCurrentState();
                if(oldState != newState){
                    this.activeRuns.add(newRun);
                    State tempState = this.nfa.getStates(newState);
                    if(tempState.isBeforeNegation()){
                        r.setBeforeNegationTimestamp(e.getTimestamp());
                    }else if(tempState.isAfterNegation()){
                        r.setAfterNegationTimestamp(e.getTimestamp());
                    }
                }else{//kleene closure
                    if(newRun.isFull){
                        //check match and output match
                        if(newRun.checkMatch()){
                            this.outputMatchForNegation(new Match(newRun, this.nfa, this.buffer), newRun);
                            Profiling.totalRunLifeTime += (System.nanoTime() - r.getLifeTimeBegin());

                        }
                    }else{
                        //check proceed
                        if(this.checkProceed(newRun)){
                            Run newerRun = this.cloneRun(newRun);
                            this.activeRuns.add(newRun);
                            newerRun.proceed();
                            if(newerRun.isComplete()){
                                this.outputMatchForNegation(new Match(r, this.nfa, this.buffer), r);
                                Profiling.totalRunLifeTime += (System.nanoTime() - r.getLifeTimeBegin());

                            }else {
                                this.activeRuns.add(newerRun);
                            }
                        }
                    }
                }



            }else{
                this.toDeleteRuns.add(r);
            }

        }
    }

    public void addRunByPartition(Run newRun) {
        if(this.activeRunsByPartition.containsKey(newRun.getPartitonId())){
            this.activeRunsByPartition.get(newRun.getPartitonId()).add(newRun);
        }else{
            ArrayList<Run> newPartition = new ArrayList<Run>();
            newPartition.add(newRun);
            this.activeRunsByPartition.put(newRun.getPartitonId(), newPartition);
        }
    }

    public void evaluateEventForPartitonContiguity (Event e, Run r)
        throws CloneNotSupportedException {
        boolean checkResult = true;
        checkResult = this.checkPredicate(e, r); // check predicate
        if(checkResult){ // the predicate if ok.
            checkResult = this.checkTimeWindow(e, r); // the time window is ok
            if(checkResult){// predicate and time window are ok
                this.buffer.bufferEvent(e);// add the event to buffer
                int oldState = 0;
                int newState = 0;
                oldState = r.getCurrentState();
                r.addEvent(e);
                newState = r.getCurrentState();
                if(oldState == newState)//kleene closure
                    if(r.isFull){
                        //check match and output match
                        if(r.checkMatch()){
                            this.outputMatch(new Match(r, this.nfa, this.buffer));
                            Profiling.totalRunLifeTime += (System.nanoTime() - r.getLifeTimeBegin());
                            this.toDeleteRuns.add(r);
                        }
                    }else{
                        //check proceed
                        if(this.checkProceed(r)){
                            Run newRun = this.cloneRun(r);
                            this.activeRuns.add(newRun);
                            this.addRunByPartition(newRun);

                            r.proceed();
                            if(r.isComplete()){
                                this.outputMatch(new Match(r, this.nfa, this.buffer));
                                Profiling.totalRunLifeTime += (System.nanoTime() - r.getLifeTimeBegin());
                                this.toDeleteRuns.add(r);

                            }
                        }
                    }


            }else{
                this.toDeleteRuns.add(r);
            }

        }else{
            this.toDeleteRuns.add(r);
        }
    }

    public void addEventToRun(Run r, Event e){
        this.buffer.bufferEvent(e);// add the event to buffer
        int oldState = 0;
        int newState = 0;
        oldState = r.getCurrentState();
        r.addEvent(e);
        newState = r.getCurrentState();
        if(oldState == newState)//kleene closure
            if(r.isFull){
                //check match and output match
                if(r.checkMatch()){
                    this.outputMatch(new Match(r, this.nfa, this.buffer));
                    Profiling.totalRunLifeTime += (System.nanoTime() - r.getLifeTimeBegin());
                    this.toDeleteRuns.add(r);
                }
            }

    }

    public void addEventToRunForNegation(Run r, Event e){
        this.buffer.bufferEvent(e);// add the event to buffer
        int oldState = 0;
        int newState = 0;
        oldState = r.getCurrentState();
        r.addEvent(e);
        newState = r.getCurrentState();
        State tempState = this.nfa.getStates(newState);

        if(tempState.isBeforeNegation()){
            r.setBeforeNegationTimestamp(e.getTimestamp());
        }else if(tempState.isAfterNegation()){
            r.setAfterNegationTimestamp(e.getTimestamp());
        }
        if(oldState == newState)//kleene closure
            if(r.isFull){
                //check match and output match
                if(r.checkMatch()){
                    this.outputMatchByPartitionForNegation(new Match(r, this.nfa, this.buffer), r);
                    Profiling.totalRunLifeTime += (System.nanoTime() - r.getLifeTimeBegin());
                    this.toDeleteRuns.add(r);
                }
            }
    }

    public Run getRun () {
        int size = runPool.size();
        for(int i = 0; i < size ; i ++){
            if (!(runPool.get(i)).isAlive()){
                return runPool.get(i);
            }
        }
        runPool.add(new Run());
        return runPool.get(size);
    }

    public void createNewRun(Event e) throws EvaluationException{
        if(this.nfa.getStates()[0].canStartWithEvent(e)){
            this.buffer.bufferEvent(e);
            Run newRun = getRun();
            newRun.initializeRun(this.nfa);
            newRun.addEvent(e);
            //this.numberOfRuns.update(1);
            Profiling.numberOfRuns ++;
            this.activeRuns.add(newRun);

        }
    }

    public void createNewRunByPartition(Event e)
        throws EvaluationException, CloneNotSupportedException{
        if(this.nfa.getStates()[0].canStartWithEvent(e)){
            this.buffer.bufferEvent(e);
            Run newRun = getRun();
            newRun.initializeRun(this.nfa);
            newRun.addEvent(e);
            //this.numberOfRuns.update(1);
            Profiling.numberOfRuns ++;
            this.activeRuns.add(newRun);
            this.addRunByPartition(newRun);

        }
    }

    public int checkPredicateOptimized(Event e, Run r){//0 for false, 1 for take or begin, 2 for proceed
        int currentState = r.getCurrentState();
        State s = this.nfa.getStates(currentState);
        if(!s.getEventType().equalsIgnoreCase(e.getEventType())){// event type check;
            return 0;
        }

        if(!s.isKleeneClosure()){
            Edge beginEdge = s.getEdges(0);
            boolean result;
            //result = firstEdge.evaluatePredicate(e, r, buffer);
            result = beginEdge.evaluatePredicate(e, r, buffer);//
            if(result){
                return 1;
            }
        }else{
            if(r.isKleeneClosureInitialized()){
                boolean result;
                result = this.checkProceedOptimized(e, r);//proceedEdge.evaluatePredicate(e, r, buffer);
                if(result){
                    return 2;
                }else{




                    Edge takeEdge = s.getEdges(1);
                    result = takeEdge.evaluatePredicate(e, r, buffer);
                    if(result){
                        return 1;
                    }
                }
            }else{
                Edge beginEdge = s.getEdges(0);
                boolean result;

                result = beginEdge.evaluatePredicate(e, r, buffer);//
                if(result){
                    return 1;
                }
            }
        }



        return 0;


    }
    /**
     * Checks whether the run needs to proceed if we add e to r
     * @param e The current event
     * @param r The run against which e is evaluated
     * @return The checking result, TRUE for OK to proceed
     */
    public boolean checkProceedOptimized(Event e, Run r){
        int currentState = r.getCurrentState();
        State s = this.nfa.getStates(currentState + 1);
        if(!s.getEventType().equalsIgnoreCase(e.getEventType())){// event type check;
            return false;
        }
        Edge beginEdge = s.getEdges(0);
        boolean result;
        result = beginEdge.evaluatePredicate(e, r, buffer);
        return result;
    }
    /**
     * Checks whether the event satisfies the predicates of a run
     * @param e the current event
     * @param r the current run
     * @return the check result
     */

    public boolean checkPredicate(Event e, Run r){
        int currentState = r.getCurrentState();
        State s = this.nfa.getStates(currentState);
        if(!s.getEventType().equalsIgnoreCase(e.getEventType())){// event type check;
            return false;
        }

        if(!s.isKleeneClosure()){
            Edge beginEdge = s.getEdges(0);
            boolean result;
            //result = firstEdge.evaluatePredicate(e, r, buffer);
            result = beginEdge.evaluatePredicate(e, r, buffer);//
            if(result){
                return true;
            }
        }else{
            if(r.isKleeneClosureInitialized()){
                Edge takeEdge = s.getEdges(1);
                boolean result;
                result = takeEdge.evaluatePredicate(e, r, buffer);
                if(result){
                    return true;
                }
            }else{
                Edge beginEdge = s.getEdges(0);
                boolean result;

                result = beginEdge.evaluatePredicate(e, r, buffer);//
                if(result){
                    return true;
                }
            }
        }



        return false;


    }
    /**
     * Checks whether the event satisfies the partition of a run, only used under partition-contiguity selection strategy
     * @param e the current event
     * @param r the current run
     * @return the check result, boolean format
     */
    public boolean checkPartition(Event e, Run r){

        if(r.getPartitonId() == e.getAttributeByName(this.nfa.getPartitionAttribute())){
            return true;
        }
        return false;
    }

    /**
     * Checks whether a kleene closure state can proceed to the next state
     * @param r the current run
     * @return the check result, boolean format
     */


    public boolean checkProceed(Run r){// cannot use previous, only use position?
        int currentState = r.getCurrentState();

        Event previousEvent = this.buffer.getEvent(r.getPreviousEventId());
        State s = this.nfa.getStates(currentState);



        Edge proceedEdge = s.getEdges(2);
        boolean result;
        result = proceedEdge.evaluatePredicate(previousEvent, r, buffer);//
        if(result){
            return true;
        }

        return false;

    }

    public boolean checkNegation(Event e) throws EvaluationException{
        if(this.nfa.getNegationState().canStartWithEvent(e)){
            return true;
        }
        return false;
    }

    public void indexNegationByPartition(Event e){
        int id = e.getAttributeByName(this.nfa.getPartitionAttribute());
        if(this.negationEventsByPartition.containsKey(id)){
            this.negationEventsByPartition.get(id).add(e);
        }else{
            ArrayList<Event> newPartition = new ArrayList<Event>();
            newPartition.add(e);
            this.negationEventsByPartition.put(id, newPartition);
        }

    }
    public boolean searchNegation(int beforeTimestamp, int afterTimestamp, ArrayList<Event> list){
        // basic idea is to use binary search on the timestamp
        int size = list.size();
        int lower = 0;
        int upper = size - 1;
        Event tempE;
        while(lower <= upper){
            tempE = list.get((lower + upper)/2);
            if(tempE.getTimestamp() >= beforeTimestamp && tempE.getTimestamp() <= afterTimestamp){
                return true;
            }
            if(tempE.getTimestamp() < beforeTimestamp){
                lower = (lower + upper)/2;
            }else {
                upper = (lower + upper)/2;
            }

        }
        return false;
    }
    public boolean searchNegationByPartition(int beforeTimestamp, int afterTimestamp, int partitionId){
        if(this.negationEventsByPartition.containsKey(partitionId)){
            ArrayList<Event> tempList = this.negationEventsByPartition.get(partitionId);
            return this.searchNegation(beforeTimestamp, afterTimestamp, tempList);

        }
        return false;
    }

    public Run cloneRun(Run r) throws CloneNotSupportedException{
        Run newRun = getRun();
        newRun = (Run)r.clone();
        Profiling.numberOfRuns ++;
        return newRun;
    }

    public boolean checkTimeWindow(Event e, Run r){
        if((e.getTimestamp() - r.getStartTimeStamp()) <= this.nfa.getTimeWindow()){
            return true;
        }
        return false;
    }

    public void outputMatch(Match m) {
        Profiling.numberOfMatches ++;
        //this.matches.addMatch(m);
        if(true){
            System.out.println("----------Here is the No." + Profiling.numberOfMatches +" match----------");
            if(Profiling.numberOfMatches == 878){
                System.out.println("debug");
            }
            System.out.println(m);
        }
    }

    public void outputMatchForNegation(Match m, Run r){
        if(this.searchNegation(r.getBeforeNegationTimestamp(), r.getAfterNegationTimestamp(), this.negationEvents)){
            Profiling.negatedMatches ++;
            System.out.println("~~~~~~~~~~~~~~~~Here is a negated match~~~~~~~~~~~~~~~");
            System.out.println(m);
        }else{

            Profiling.numberOfMatches ++;
            //this.matches.addMatch(m);
            if(true){
                System.out.println("----------Here is the No." + Profiling.numberOfMatches +" match----------");
                if(Profiling.numberOfMatches == 878){
                    System.out.println("debug");
                }
                System.out.println(m);
            }

        }

    }

    public void outputMatchByPartitionForNegation(Match m, Run r){
        if(this.searchNegationByPartition(r.getBeforeNegationTimestamp(), r.getAfterNegationTimestamp(), r.getPartitonId())){
            Profiling.negatedMatches ++;
            System.out.println("~~~~~~~~~~~~~~~~Here is a negated match~~~~~~~~~~~~~~~");
            System.out.println(m);
        }else{

            Profiling.numberOfMatches ++;
            //this.matches.addMatch(m);
            if(true){
                System.out.println("----------Here is the No." + Profiling.numberOfMatches +" match----------");
                if(Profiling.numberOfMatches == 878){
                    System.out.println("debug");
                }
                System.out.println(m);
            }

        }

    }

    /**
     * Deletes runs violating the time window
     * @param currentTime current time
     * @param timeWindow time window of the query
     * @param delayTime specified delay period, any run which has been past the time window by this value would be deleted.
     */
    public void deleteRunsOverTimeWindow(int currentTime, int timeWindow, int delayTime){
        int size = this.activeRuns.size();
        Run tempRun = null;
        for(int i = 0; i < size; i ++){
            tempRun = this.activeRuns.get(i);
            if(!tempRun.isFull&&tempRun.getStartTimeStamp() + timeWindow + delayTime < currentTime){
                this.toDeleteRuns.add(tempRun);
                Profiling.numberOfRunsOverTimeWindow ++;

            }
        }
    }

    /**
     * Cleans useless runs
     */
    public void cleanRuns(){
        int size = this.toDeleteRuns.size();
        Run tempRun = null;
        for(int i = 0; i < size; i ++){
            tempRun = toDeleteRuns.get(0);
            Profiling.totalRunLifeTime += (System.nanoTime() - tempRun.getLifeTimeBegin());
            tempRun.resetRun();
            this.activeRuns.remove(tempRun);
            this.toDeleteRuns.remove(0);
            Profiling.numberOfRunsCutted ++;
        }
    }

    /**
     * Cleans useless runs by partition.
     */
    public void cleanRunsByPartition(){

        int size = this.toDeleteRuns.size();
        Run tempRun = null;
        ArrayList<Run> partitionedRuns = null;
        for(int i = 0; i < size; i ++){
            tempRun = toDeleteRuns.get(0);
            Profiling.totalRunLifeTime += (System.nanoTime() - tempRun.getLifeTimeBegin());
            tempRun.resetRun();
            this.activeRuns.remove(tempRun);
            this.toDeleteRuns.remove(0);
            Profiling.numberOfRunsCutted ++;
            partitionedRuns = this.activeRunsByPartition.get(tempRun.getPartitonId());
            partitionedRuns.remove(tempRun);
            if(partitionedRuns.size() == 0){
                this.activeRunsByPartition.remove(partitionedRuns);
            }

        }
    }
}
