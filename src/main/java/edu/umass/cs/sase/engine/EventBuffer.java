package edu.umass.cs.sase.engine;

import java.util.HashMap;

import edu.umass.cs.sase.stream.Event;

public class EventBuffer {

    HashMap<Integer, Event> buffer;
    public EventBuffer(){
        buffer = new HashMap<Integer, Event>();
    }

    public Event getEvent(int Id){
        return buffer.get(Id);
    }

    public void bufferEvent(Event e){
        if(buffer.get(e.getId()) == null){
            buffer.put(e.getId(), e);
        }
    }
}
