package edu.umass.cs.sase.stream;


/**
 * This class represents a kind of event.
 * @author haopeng
 *
 */
public class ABCEvent implements Event{
    /**
     * Event id
     */
    int id;

    /**
     * Event timestamp
     */
    int timestamp;

    /**
     * event type
     */
    String eventType;

    /**
     * Price, an attribute
     */
    int price;

    /**
     * Constructor
     */
    public  ABCEvent(int i, int ts, String et, int p){
        id = i;
        timestamp = ts;
        eventType = et;
        price = p;

    }

    /**
     * @return the cloned event
     */
    public Object clone(){
        ABCEvent o = null;
        try {
            o = (ABCEvent)super.clone();
        } catch (CloneNotSupportedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return o;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * @return the timestamp
     */
    public int getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the eventType
     */
    public String getEventType() {
        return eventType;
    }

    /**
     * @param eventType the eventType to set
     */
    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    /**
     * @return the price
     */
    public int getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(int price) {
        this.price = price;
    }

    public String toString(){
        return "ABC:("+ id + "," + timestamp
            + "," + eventType + "," + price + ")";
    }

    /* (non-Javadoc)
     * @see edu.umass.cs.sase.mvc.model.Event#getAttributeByName(java.lang.String)
     */
    public int getAttributeByName(String attributeName) {

        if(attributeName.equalsIgnoreCase("price"))
            return price;
        if(attributeName.equalsIgnoreCase("id"))
            return this.id;
        if(attributeName.equalsIgnoreCase("timestamp"))
            return this.timestamp;

        return -1;
    }

    /* (non-Javadoc)
     * @see edu.umass.cs.sase.mvc.model.Event#getAttributeByNameString(java.lang.String)
     */
    public String getAttributeByNameString(String attributeName) {
        // TODO Auto-generated method stub
        return null;
    }
    /* (non-Javadoc)
     * @see edu.umass.cs.sase.mvc.model.Event#getAttributeValueType(java.lang.String)
     */
    public int getAttributeValueType(String attributeName) {
        // TODO Auto-generated method stub
        return 0;
    }

    /* (non-Javadoc)
     * @see edu.umass.cs.sase.mvc.model.Event#getAttributeByNameDouble(java.lang.String)
     */
    public double getAttributeByNameDouble(String attributeName) {
        // TODO Auto-generated method stub
        return 0;
    }
}
