package edu.umass.cs.sase.stream;

/**
 * This class represents a stream.
 * @author haopeng
 *
 */
public class Stream {

    /**
     * Events in the stream
     */
    Event[] events;

    /**
     * The number of events in the stream
     */
    int size;

    /**
     * A counter
     */
    int counter;

    public Stream (Event[] events) {
        this.events = events;
        this.setSize (events.length);
        this.counter = -1;
    }

    /**
     *
     * @return the next event in the stream
     */
    public Event popEvent(){
        counter++;
        if(counter < size){
            return events[counter];
        }
        else
            return null;
    }

    public Event[] getEvents() {
        return events;
    }

    /**
     * @return the size
     */
    public int getSize() {
        return size;
    }

    /**
     * @param size the size to set
     */
    public void setSize(int size) {
        this.size = size;
    }

    /**
     * @return the counter
     */
    public int getCounter() {
        return counter;
    }

    /**
     * @param counter the counter to set
     */
    public void setCounter(int counter) {
        this.counter = counter;
    }

}
