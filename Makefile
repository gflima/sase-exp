.PHONY: all
all:
	mvn -q package

.PHONY: run
run: all
	./sase ${query} ${strat}
